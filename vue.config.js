module.exports = {
    productionSourceMap: false,
    devServer: {
        proxy: {
            '/': {
                target: 'http://platform/',
                changeOrigin: true,
                pathRewrite: {
                    '^/': ''
                }
            }
        }
    }
}