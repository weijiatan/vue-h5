import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import upload from '@/components/common/upload.vue'

// PX转REM
import 'amfe-flexible/index.js'
// 适配桌面端
import '@vant/touch-emulator'

import Vant from 'vant'
import 'vant/lib/index.css'

// // 路由权限验证
router.beforeEach((to, from, next) => {
    const params = to.query;
    params.token && localStorage.setItem('systemMember', { token: params.token });
    next();
});

createApp(App)
    .use(store)
    .use(router)
    .use(Vant)
    .component('solux-upload', upload)
    .mount('#app')
