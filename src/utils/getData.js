import axios from '../utils/axios';

// 上传接口
export const uploadUrl = '/api/system/common/upload.html';
// 上传附件
export const uploadfile = (formData) => axios.post('/api/system/common/upload.html', formData);